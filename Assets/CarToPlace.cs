﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CarToPlace : MonoBehaviour
{
    public Transform[] Positions;
    Tweener sq;
    // Start is called before the first frame update
    void Start()
    {
        sq = this.transform.DORotate(new Vector3(0, 180, 0), 5, RotateMode.WorldAxisAdd).SetLoops(-1,LoopType.Incremental);
    }

    void StartParking()
    {
        
    }
    // Update is called once per frame
    void Update()
    {
        
        this.transform.TransformDirection(Vector3.forward);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Points")
        {
            transform.LookAt(this.transform.position);
        }
    }
}
